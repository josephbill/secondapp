package com.example.secondapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    //
    Button btnJSON;
    TextView textView;
    //declare volley request Queue
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnJSON = findViewById(R.id.btnJson);
        textView = findViewById(R.id.text);

        //new instance of RequestQueue from Volley
//        requestQueue =  Volley.newRequestQueue(this);
        //using singleton pattern
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        btnJSON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCountriesCovid();
            }
        });


    }


    private void getCountriesCovid(){
        //define the API URL Distribution Link
        String URL  = "https://api.covid19api.com/summary";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{

                    JSONArray jsonArray = response.getJSONArray("Countries");
                    for (int i=0; i < jsonArray.length(); i++){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String country = jsonObject1.getString("Country");
                        int totalRec = jsonObject1.getInt("TotalRecovered");

                        //append to text
                        textView.append(" Country: " + country + " " + " rec: " + totalRec);
                        Log.d("details ", "Country and Rec " + country);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Something went wrong,Try again later or check internet connection", Toast.LENGTH_SHORT).show();
            }
        });

        //add request to queue
        requestQueue.add(request);

    }



    public  void recycler(View v){
        Intent intent = new Intent(MainActivity.this,VolleyRecyclerActivity.class);
        startActivity(intent);
    }

    public  void fastactivity(View v){
        Intent intent = new Intent(MainActivity.this,FastandroidNetworking_Activity.class);
        startActivity(intent);
    }


    public  void location(View v){
        Intent intent = new Intent(MainActivity.this,LocationActivity.class);
        startActivity(intent);
    }


    public  void map(View v){
        Intent intent = new Intent(MainActivity.this,MapsActivity.class);
        startActivity(intent);    }

}
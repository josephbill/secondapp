package com.example.secondapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.secondapp.R;
import com.example.secondapp.model.RecyclerModel;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    //
    Context context;
    ArrayList<RecyclerModel> recyclerModels;

    //constructor
    public RecyclerAdapter(Context context, ArrayList<RecyclerModel> recyclerModelArrayList){
        this.context = context;
        recyclerModels = recyclerModelArrayList;
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        ImageView imageView;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textName);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }




    @NonNull
    @Override
    public RecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.recycled_item, parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerViewHolder holder, int position) {
             RecyclerModel recyclerModel = recyclerModels.get(position);

             //bind data
             holder.textView.setText("Uploaded By: " + recyclerModel.getTextName());
             //glide to load image
             Glide.with(context)
                .load(recyclerModel.getImageUrl())
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return recyclerModels.size();
    }
}

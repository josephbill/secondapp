package com.example.secondapp.model;

import android.content.Context;

public class RecyclerModel {
    //declare variables
    Context context;
    private String textName;
    private String imageUrl;

    //constructor
    public RecyclerModel(Context context,String textName,String imageUrl){
        this.textName = textName;
        this.context = context;
        this.imageUrl = imageUrl;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

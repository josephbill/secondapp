package com.example.secondapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LocationActivity extends AppCompatActivity {
    //declare views
    TextView textView;
    //variable address
    String address;
    //variables LAT AND LNG
    double lat,lng;
    //Geocoder
    Geocoder geocoder;
    //model list
    List<Address> addresses;

    private static int REQUEST_CODE_LOCATION_PERMISSION = 2222;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        //view identification
        textView = findViewById(R.id.textLoc);

        //new instance of the Geocoder class
        geocoder = new Geocoder(this, Locale.getDefault());

        //permission popup
        permissionPop();
    }


    //permission to get location
    private void permissionPop(){
        //get the location services of the device via the LocationManager class
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        //check if providers ARE enabled
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                 //alert dialog to ask user for permission to enable the above services
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //customizing popup
            builder.setTitle("Location Permissions");
            builder.setIcon(R.drawable.ic_launcher_background);

            //set controls
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //intent to start a service
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(LocationActivity.this, "App may not work well", Toast.LENGTH_SHORT).show();
                }
            });

            //show your alert dialog
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();


        } else {
            Toast.makeText(this, "Location is on", Toast.LENGTH_SHORT).show();
        }

    }

    public void getUserLoc(View v){
        getLocation();
    }

    //gets users current location
    private void getLocation(){
        new AlertDialog.Builder(this)
                .setTitle("Allow me to get your current location")
                .setMessage("this will allow for better services")

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                            //from this point it will move to the onRequestPermissionResult method
                            ActivityCompat.requestPermissions(
                                    LocationActivity.this,
                                    new String[]{ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION
                            );

                        } else {
                            //if the permission is already set
                            getCurrentLocation();
                        }

                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         Toast.makeText(LocationActivity.this,"location not on app may not work properly",Toast.LENGTH_LONG).show();
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }


    //onrequestpermission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION  && grantResults.length > 0){
            getCurrentLocation();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    //using Location API to fetch location
    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        //create a new instance of the LocationRequest class

        LocationRequest locationRequest = new LocationRequest();

        //set metadata
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(LocationActivity.this)
                .requestLocationUpdates(locationRequest, new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(LocationActivity.this)
                                .removeLocationUpdates(this);
                        //check for location result
                        if (locationResult != null && locationResult.getLocations().size() > 0){
                            //set up location index which will allow us to get the location lat and longitude
                            int latestlocationIndex = locationResult.getLocations().size() - 1; //removing the last known location from our  list

                            //getting lat anf lng
                            lat = locationResult.getLocations().get(latestlocationIndex).getLatitude();
                            lng = locationResult.getLocations().get(latestlocationIndex).getLongitude();

                            Log.d("bill","lat is " + lat + " lon is "  + lng);

                            //using geocoder to convert lat and lng
                            try {
                                addresses = geocoder.getFromLocation(lat,lng,1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            //decode address further
                            //using the list i can access the details of the user current location
                            //address
                            address = addresses.get(0).getAddressLine(0);
                            //get other info about users current location
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName();

                            //set text to textview
                            textView.setText("User's location: " + " " + address);

                        }
                    }
                }, Looper.getMainLooper());


    }
}
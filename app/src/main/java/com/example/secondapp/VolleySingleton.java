package com.example.secondapp;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
    //define a ref to this class
    private static VolleySingleton volleySingleton;
    //define RequestQueue
    private RequestQueue requestQueue;

    //constructor
    public VolleySingleton(Context context){
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }


    public static synchronized VolleySingleton getInstance(Context context){
        if (volleySingleton == null){
            volleySingleton = new VolleySingleton(context);
        }

        return volleySingleton;
    }


    public RequestQueue getRequestQueue(){
        return requestQueue;
    }



}

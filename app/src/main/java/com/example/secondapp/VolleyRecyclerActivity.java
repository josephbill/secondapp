package com.example.secondapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.secondapp.adapter.RecyclerAdapter;
import com.example.secondapp.model.RecyclerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyRecyclerActivity extends AppCompatActivity {

    //declare our tools
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<RecyclerModel> recyclerModels;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley_recycler);

        //recyclerview
        recyclerView = findViewById(R.id.recyclerVolley);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //new instance of ArrayList
        recyclerModels = new ArrayList<>();

        //new instance of volley
        requestQueue = Volley.newRequestQueue(this);

        //method to fetch data
        fetchImages();


    }

    private void fetchImages(){
        String URL = "https://pixabay.com/api/?key=5303976-fd6581ad4ac165d1b75cc15b3&q=kitten&image_type=photo&pretty=true";

        //creating the request
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                  try {

                      //get array
                      JSONArray jsonArray = response.getJSONArray("hits");
                      for(int i=0; i < jsonArray.length(); i++){
                          JSONObject jsonObject = jsonArray.getJSONObject(i);
                          String imagePath = jsonObject.getString("previewURL");
                          String imageText = jsonObject.getString("user");

                          recyclerModels.add(new RecyclerModel(VolleyRecyclerActivity.this,imageText,imagePath));

                      }

                      //adapter
                      recyclerAdapter = new RecyclerAdapter(VolleyRecyclerActivity.this,recyclerModels);
                      recyclerView.setAdapter(recyclerAdapter);


                  } catch (JSONException e){
                      e.printStackTrace();
                  }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(request);
    }

}
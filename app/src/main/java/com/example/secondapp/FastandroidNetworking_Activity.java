package com.example.secondapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FastandroidNetworking_Activity extends AppCompatActivity {
    //declare views
    TextView textget;
    Button btnGet,btnPost;
    TextInputEditText editAge,editName;
    String last_name,name;
    SweetAlertDialog errorDialog,successDialog,progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fastandroid_networking_);

        //intialize fast android networking
        AndroidNetworking.initialize(getApplicationContext());

        errorDialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
        successDialog = new SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE);
        progressDialog = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);

        //view identification
        textget = findViewById(R.id.textget);
        btnGet = findViewById(R.id.btnGet);
        btnPost = findViewById(R.id.postData);
        editAge = findViewById(R.id.editAge);
        editName = findViewById(R.id.editName);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postData();
            }
        });


    }


    private void getData(){
        //variable URL
        String URL = "https://api.covid19api.com/summary";
        //give user progress
        progressDialog.setTitle("Fetching Data");
        progressDialog.setTitleText("Wait a bit");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        //we handle getting a response
        AndroidNetworking.get(URL)
                .addQueryParameter("Slug","kenya")
                .setTag("get")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {

                            JSONArray jsonArray = response.getJSONArray("Countries");
                            for (int i=0; i < jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String slug = jsonObject.getString("Slug");
                                int newConfirmed = jsonObject.getInt("NewConfirmed");


                                //set text
                                textget.append("Slug is: " + slug + " " + "New Confirmed Cases: " + newConfirmed);

                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        errorDialog.setTitle("Error Occurred");
                        errorDialog.setTitleText(String.valueOf(anError));
                        errorDialog.setCancelable(true);
                        errorDialog.setCanceledOnTouchOutside(false);
                        errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                errorDialog.dismiss();
                            }
                        });
                        errorDialog.show();
                    }
                });

    }

    private void postData(){
        //url variable
        String URL = "https://reqres.in/api/users";
        //capture user input
        name = editName.getText().toString().trim();
        last_name = editAge.getText().toString().trim();
        //post Data
        AndroidNetworking.post(URL)
                .addBodyParameter("first_name",name)
                .addBodyParameter("last_name",last_name)
                .setTag("post")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        successDialog.setTitleText("Successfully submitted");
                        successDialog.setTitle("Post");
                        successDialog.setCancelable(true);
                        successDialog.setCanceledOnTouchOutside(false);
                        successDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                successDialog.dismiss();
                            }
                        });
                        successDialog.show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        errorDialog.setTitle("Error Occurred");
                        errorDialog.setTitleText(String.valueOf(anError));
                        errorDialog.setCancelable(true);
                        errorDialog.setCanceledOnTouchOutside(false);
                        errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                errorDialog.dismiss();
                            }
                        });
                        errorDialog.show();
                    }
                });


    }

}